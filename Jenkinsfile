pipeline {

    agent any

    parameters {
        string(name: 'tests', defaultValue: 'RunCukes', description: 'cucumber tests')
        string(name: 'url', defaultValue: 'http://toolslist.safebear.co.uk:8080', description: 'test environment')
        string(name: 'browser', defaultValue: 'chrome', description: 'chrome headless')
        string(name: 'sleep', defaultValue: '0', description: 'set sleep commands to zero')
    }

    triggers { pollSCM('1 0 * * *') } //poll the source code repo every minute.

    stages {

        stage('BDD testing') {
            steps {
                bat "mvn -Dtest=${params.tests} test -Durl=${params.url} -Dbrowser=${params.browser} -Dsleep=${params.sleep}"
            }

            post {
                always {
                    publishHTML([
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : false,
                            reportDir            : 'target/cucumber-reports',
                            reportFiles          : 'report.html',
                            reportName           : 'BDD report',
                            reportTitles         : ''])
                }
            }
        }
    }
}