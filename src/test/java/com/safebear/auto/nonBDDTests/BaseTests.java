package com.safebear.auto.nonBDDTests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.pages.locators.AddToolsPage;
import com.safebear.auto.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import sun.rmi.runtime.Log;

public class BaseTests {

    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;
    AddToolsPage addToolsPage;

    @BeforeTest
    public void setUp() {
        driver = Properties.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
        addToolsPage = new AddToolsPage(driver);
    }

    @AfterTest
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

}
