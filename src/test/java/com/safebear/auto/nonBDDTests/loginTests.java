package com.safebear.auto.nonBDDTests;

import com.safebear.auto.utils.Properties;
import org.testng.Assert;
import org.testng.annotations.Test;

public class loginTests extends BaseTests {

    @Test
    public void validLogin() {
        //1.Action Go to login page
        driver.get(Properties.getUrl());
        //1.Expected check I'm on login
        Assert.assertEquals(loginPage.getTitle(),loginPage.getExpectedPageTitle());
        //2.Action login as a valid user
        loginPage.login("tester", "letmein");
        //2.Expected check I'm logged in by checking for success message
        Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page");
        Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Success"));
    }

    @Test
    public void invalidLogin() {
        //1.Action Go to login page
        driver.get(Properties.getUrl());
        //1.Expected check I'm on login
        Assert.assertEquals(loginPage.getTitle(),loginPage.getExpectedPageTitle());
        //2.Action login as a invalid user
        loginPage.login("hacker", "letmein");
        //2.Expected check I'm not logged in by checking for error message
        Assert.assertEquals(loginPage.getTitle(),"Login Page");
        Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains("incorrect"));
    }

    @Test
    public void addTool() {
        //1.Action Go to login page
        driver.get(Properties.getUrl());
        //1.Expected check I'm on login
        Assert.assertEquals(loginPage.getTitle(),loginPage.getExpectedPageTitle());
        //2.Action login as a valid user
        loginPage.login("tester", "letmein");
        //2.Expected check I'm logged in by checking for success message
        Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page");
        Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Success"));
        //3.Action add tool
        toolsPage.addItem();
        addToolsPage.enterName();
        addToolsPage.enterUse();
        addToolsPage.enterWebsite();
        addToolsPage.submitForm();
        //3.Expected check if tool has been added
        //add assertion here

    }

}
