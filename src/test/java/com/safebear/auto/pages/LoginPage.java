package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
    //Creating locator object
    LoginPageLocators locators = new LoginPageLocators();

    //
    @NonNull
    WebDriver driver;
    String expectedPageTitle = "Login Page";

    public String getTitle() {
        return driver.getTitle();
    }

    public String getExpectedPageTitle() {
        return expectedPageTitle;
    }

    public void login(String username, String password) {
        driver.findElement(locators.getUsernameFieldLocator()).sendKeys(username);
        driver.findElement(locators.getPasswordFieldLocator()).sendKeys(password);
        driver.findElement(locators.getLoginButtonLocator()).click();
    }

    public String checkForFailedLoginWarning() {
        return driver.findElement(locators.getFailedLoginMessage()).getText();
    }

}
