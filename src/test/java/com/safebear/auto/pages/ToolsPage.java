package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {

    //bringing in existing webdriver
    @NonNull
    WebDriver driver;

    //Creating toolspagelocator object
    ToolsPageLocators toolsPageLocators = new ToolsPageLocators();

    public  String getPageTitle() {
        return driver.getTitle();
    }

    public String checkForLoginSuccessfulMessage() {
        return driver.findElement(toolsPageLocators.getSuccessfulLoginMessage()).getText();
    }

    public void addItem() {
        driver.findElement(toolsPageLocators.getAddButtonLocator()).click();
    }

    public void removeItem() {
        driver.findElement(toolsPageLocators.getRemoveButtonLocator()).click();
    }


}
