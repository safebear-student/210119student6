package com.safebear.auto.pages.locators;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class AddToolsPage {

    //bringing in existing webdriver
    @NonNull
    WebDriver driver;

    //creating new addToolsPageLocator object
    AddToolsPageLocators addToolsPageLocators = new AddToolsPageLocators();

    public void enterName() {
        driver.findElement(addToolsPageLocators.getNameInputField()).sendKeys("Mailinator");
    }

    public void enterUse() {
        driver.findElement(addToolsPageLocators.getUseInputField()).sendKeys("fake emails");
    }

    public void enterWebsite() {
        driver.findElement(addToolsPageLocators.getWebSiteInputField()).sendKeys("mailinator.com");
    }

    public void submitForm() {
        driver.findElement(addToolsPageLocators.getSubmitButton()).click();
    }



}

