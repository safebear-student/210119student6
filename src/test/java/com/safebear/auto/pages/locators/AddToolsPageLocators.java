package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class AddToolsPageLocators {

    // input fields

    private By nameInputField = By.id("name");

    private By useInputField = By.id("use");

    private By webSiteInputField = By.id("website");

    private By submitButton = By.xpath("//button[contains(text(),\"Accept\")]");

}

