package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {

    @Test
    public void testEmployee() {

        Employee hannah = new Employee();
        Employee bob = new Employee();
        SalesEmployee victoria = new SalesEmployee();
        OfficeEmployee harry = new OfficeEmployee();

        hannah.setEmployed(true);
        bob.setEmployed(false);
        victoria.setEmployed(true);
        victoria.setSalary(39000);
        harry.demote();

        victoria.changeCar("Mercedes");


        System.out.println("Hannah employment state: " + hannah.isEmployed());
        System.out.println("Bob employment state: " + bob.isEmployed());
        System.out.println("Victoria employment state: " + victoria.isEmployed());
        System.out.println("Victoria's salary is £" + victoria.getSalary() + "pa");
        System.out.println("Victoria's car: " + victoria.car);
        System.out.println("Harry's employment status: " + harry.isEmployed());
        System.out.println("Harry's senior level? " + harry.isSenior());

    }
}
