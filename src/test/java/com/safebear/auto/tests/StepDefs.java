package com.safebear.auto.tests;

import com.safebear.auto.Tool;
import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.pages.locators.LoginPageLocators;
import com.safebear.auto.utils.Properties;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class StepDefs {

    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp() {
        driver = Properties.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @Given("^I nav to the login page$")
    public void i_nav_to_the_login_page() throws Throwable {
        driver.get(Properties.getUrl());
        Assert.assertEquals(loginPage.getTitle(), loginPage.getExpectedPageTitle(),"We're not on the Login Page or its title has changed");
    }

    @When("^I enter login details for '(.+)'$")
    public void i_enter_login_details_for_a_USER(String userType) throws Throwable {
        switch (userType) {
            case "invalidUser":
                loginPage.login("attacker", "letmein");
                break;
            case "validUser":
                loginPage.login("tester", "letmein");
                break;
            default:
                Properties.capturescreenshot(driver, Properties.generateScreenShotFileName());
                Assert.fail("The test data is wrong - the only values that can be accepted are 'invalidUser' or 'validUser'");
                break;
        }
    }

    @Then("^I will see the message: '(.+)'$")
    public void i_will_see_the_validation_MESSAGE(String validationMessage) throws Throwable {
        switch (validationMessage) {
            case "Login Successful":
                Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(validationMessage));
                break;
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains(validationMessage));
                break;
            default:
                Assert.fail("The test data is wrong");
                break;
        }
    }
}
